test_that("multiplication works", {
  expect_equal(2 * 2, 4)
})

library(justDesertsR)

test_that("Generalized Proportionality works", {
  y = "y"
  R = c("r1", "r2")
  NR = c("nr1", "nr2")

  gp <- genProportion(data = JustDesertsDataset[1:5, ],
                      y = "y",
                      R = c("r1", "r2"),
                      NR = c("nr1", "nr2"))

  # assign everyone observation 1's responsibility vector
  JustDesertsDataset_copy <- JustDesertsDataset[1:5, ]
  JustDesertsDataset_copy$r1 <- JustDesertsDataset$r1[1]
  JustDesertsDataset_copy$r2 <- JustDesertsDataset$r2[1]
  # model expected incomes
  model <- stats::lm(stats::as.formula(paste0(y, " ~ ",
                                              paste(NR,
                                                    R,
                                                    sep = "+",
                                                    collapse =  "+"))),
                     # weights = weights,
                     data = JustDesertsDataset[1:5, ])
  summary(model)
  JustDesertsDataset_copy$g <- predict(model,
                                       newdata = JustDesertsDataset_copy)
  g1 <- mean(JustDesertsDataset_copy$g)

  # assign everyone observation 2's responsibility vector
  JustDesertsDataset_copy <- JustDesertsDataset[1:5, ]
  JustDesertsDataset_copy$r1 <- JustDesertsDataset$r1[2]
  JustDesertsDataset_copy$r2 <- JustDesertsDataset$r2[2]
  # model expected incomes
  model <- stats::lm(stats::as.formula(paste0(y, " ~ ",
                                              paste(NR,
                                                    R,
                                                    sep = "+",
                                                    collapse =  "+"))),
                     # weights = weights,
                     data = JustDesertsDataset[1:5, ])
  summary(model)
  JustDesertsDataset_copy$g <- predict(model,
                                       newdata = JustDesertsDataset_copy)
  g2 <- mean(JustDesertsDataset_copy$g)

  # assign everyone observation 3's responsibility vector
  JustDesertsDataset_copy <- JustDesertsDataset[1:5, ]
  JustDesertsDataset_copy$r1 <- JustDesertsDataset$r1[3]
  JustDesertsDataset_copy$r2 <- JustDesertsDataset$r2[3]
  # model expected incomes
  model <- stats::lm(stats::as.formula(paste0(y, " ~ ",
                                              paste(NR,
                                                    R,
                                                    sep = "+",
                                                    collapse =  "+"))),
                     # weights = weights,
                     data = JustDesertsDataset[1:5, ])
  summary(model)
  JustDesertsDataset_copy$g <- predict(model,
                                       newdata = JustDesertsDataset_copy)
  g3 <- mean(JustDesertsDataset_copy$g)

  # assign everyone observation 4's responsibility vector
  JustDesertsDataset_copy <- JustDesertsDataset[1:5, ]
  JustDesertsDataset_copy$r1 <- JustDesertsDataset$r1[4]
  JustDesertsDataset_copy$r2 <- JustDesertsDataset$r2[4]
  # model expected incomes
  model <- stats::lm(stats::as.formula(paste0(y, " ~ ",
                                              paste(NR,
                                                    R,
                                                    sep = "+",
                                                    collapse =  "+"))),
                     # weights = weights,
                     data = JustDesertsDataset[1:5, ])
  summary(model)
  JustDesertsDataset_copy$g <- predict(model,
                                       newdata = JustDesertsDataset_copy)
  g4 <- mean(JustDesertsDataset_copy$g)

  # assign everyone observation 5's responsibility vector
  JustDesertsDataset_copy <- JustDesertsDataset[1:5, ]
  JustDesertsDataset_copy$r1 <- JustDesertsDataset$r1[5]
  JustDesertsDataset_copy$r2 <- JustDesertsDataset$r2[5]
  # model expected incomes
  model <- stats::lm(stats::as.formula(paste0(y, " ~ ",
                                              paste(NR,
                                                    R,
                                                    sep = "+",
                                                    collapse =  "+"))),
                     # weights = weights,
                     data = JustDesertsDataset[1:5, ])
  summary(model)
  JustDesertsDataset_copy$g <- predict(model,
                                       newdata = JustDesertsDataset_copy)
  g5 <- mean(JustDesertsDataset_copy$g)

  sum_g <- g1 + g2 + g3 + g4 + g5

  # calculate fair incomes
  y1 <- (g1 / sum_g) * sum(JustDesertsDataset[1:5, ]$y)
  y2 <- (g2 / sum_g) * sum(JustDesertsDataset[1:5, ]$y)
  y3 <- (g3 / sum_g) * sum(JustDesertsDataset[1:5, ]$y)
  y4 <- (g4 / sum_g) * sum(JustDesertsDataset[1:5, ]$y)
  y5 <- (g5 / sum_g) * sum(JustDesertsDataset[1:5, ]$y)

  JustDesertsDataset_copy$z <- c(y1, y2, y3, y4, y5)

  testthat::expect_equal(gp, JustDesertsDataset_copy$z)
})



test_that("Egalitarian Equivalance works", {
  y = "y"
  R = c("r1", "r2")
  NR = c("nr1", "nr2")

  ee <- egalEquivalence(data = JustDesertsDataset[1:5, ],
                      y = "y",
                      R = c("r1", "r2"),
                      NR = c("nr1", "nr2"))

  # assign everyone observation 1's responsibility vector
  JustDesertsDataset_copy <- JustDesertsDataset[1:5, ]
  JustDesertsDataset_copy$nr1 <- mean(JustDesertsDataset$nr1)
  JustDesertsDataset_copy$nr2 <- mean(JustDesertsDataset$nr2)

  # model expected incomes
  model <- stats::lm(stats::as.formula(paste0(y, " ~ ",
                                              paste(NR,
                                                    R,
                                                    sep = "+",
                                                    collapse =  "+"))),
                     # weights = weights,
                     data = JustDesertsDataset[1:5, ])
  summary(model)
  JustDesertsDataset_copy$z.pre <- predict(model,
                                       newdata = JustDesertsDataset_copy)
  cee <- (sum(JustDesertsDataset_copy$y) -
            sum(JustDesertsDataset_copy$z.pre)) /
    nrow(JustDesertsDataset_copy)
  JustDesertsDataset_copy$z <- JustDesertsDataset_copy$z.pre + cee

  testthat::expect_equal(ee, JustDesertsDataset_copy$z)
})



# test_that("Egalitarian Equivalence_2 works", {
#   y = "y"
#   R = c("r1", "r2")
#   NR = c("nr1", "nr2")
#
#   ee_2 <- egalEquivalence_2(data = JustDesertsDataset[1:5, ],
#                        y = "y",
#                        R = c("r1", "r2"),
#                        NR = c("nr1", "nr2"))
#
#   # assign everyone observation 1's responsibility vector
#   JustDesertsDataset_copy <- JustDesertsDataset[1:5, ]
#   JustDesertsDataset_copy$r1 <- JustDesertsDataset$r1[1]
#   JustDesertsDataset_copy$r2 <- JustDesertsDataset$r2[1]
#   # model expected incomes
#   model <- stats::lm(stats::as.formula(paste0(y, " ~ ",
#                                               paste(NR,
#                                                     R,
#                                                     sep = "+",
#                                                     collapse =  "+"))),
#                      # weights = weights,
#                      data = JustDesertsDataset[1:5, ])
#   summary(model)
#   JustDesertsDataset_copy$g <- predict(model,
#                                        newdata = JustDesertsDataset_copy)
#   g1 <- mean(JustDesertsDataset_copy$g)
#
#   # assign everyone observation 2's responsibility vector
#   JustDesertsDataset_copy <- JustDesertsDataset[1:5, ]
#   JustDesertsDataset_copy$r1 <- JustDesertsDataset$r1[2]
#   JustDesertsDataset_copy$r2 <- JustDesertsDataset$r2[2]
#   # model expected incomes
#   model <- stats::lm(stats::as.formula(paste0(y, " ~ ",
#                                               paste(NR,
#                                                     R,
#                                                     sep = "+",
#                                                     collapse =  "+"))),
#                      # weights = weights,
#                      data = JustDesertsDataset[1:5, ])
#   summary(model)
#   JustDesertsDataset_copy$g <- predict(model,
#                                        newdata = JustDesertsDataset_copy)
#   g2 <- mean(JustDesertsDataset_copy$g)
#
#   # assign everyone observation 3's responsibility vector
#   JustDesertsDataset_copy <- JustDesertsDataset[1:5, ]
#   JustDesertsDataset_copy$r1 <- JustDesertsDataset$r1[3]
#   JustDesertsDataset_copy$r2 <- JustDesertsDataset$r2[3]
#   # model expected incomes
#   model <- stats::lm(stats::as.formula(paste0(y, " ~ ",
#                                               paste(NR,
#                                                     R,
#                                                     sep = "+",
#                                                     collapse =  "+"))),
#                      # weights = weights,
#                      data = JustDesertsDataset[1:5, ])
#   summary(model)
#   JustDesertsDataset_copy$g <- predict(model,
#                                        newdata = JustDesertsDataset_copy)
#   g3 <- mean(JustDesertsDataset_copy$g)
#
#   # assign everyone observation 4's responsibility vector
#   JustDesertsDataset_copy <- JustDesertsDataset[1:5, ]
#   JustDesertsDataset_copy$r1 <- JustDesertsDataset$r1[4]
#   JustDesertsDataset_copy$r2 <- JustDesertsDataset$r2[4]
#   # model expected incomes
#   model <- stats::lm(stats::as.formula(paste0(y, " ~ ",
#                                               paste(NR,
#                                                     R,
#                                                     sep = "+",
#                                                     collapse =  "+"))),
#                      # weights = weights,
#                      data = JustDesertsDataset[1:5, ])
#   summary(model)
#   JustDesertsDataset_copy$g <- predict(model,
#                                        newdata = JustDesertsDataset_copy)
#   g4 <- mean(JustDesertsDataset_copy$g)
#
#   # assign everyone observation 5's responsibility vector
#   JustDesertsDataset_copy <- JustDesertsDataset[1:5, ]
#   JustDesertsDataset_copy$r1 <- JustDesertsDataset$r1[5]
#   JustDesertsDataset_copy$r2 <- JustDesertsDataset$r2[5]
#   # model expected incomes
#   model <- stats::lm(stats::as.formula(paste0(y, " ~ ",
#                                               paste(NR,
#                                                     R,
#                                                     sep = "+",
#                                                     collapse =  "+"))),
#                      # weights = weights,
#                      data = JustDesertsDataset[1:5, ])
#   summary(model)
#   JustDesertsDataset_copy$g <- predict(model,
#                                        newdata = JustDesertsDataset_copy)
#   g5 <- mean(JustDesertsDataset_copy$g)
#
#   sum_g <- g1 + g2 + g3 + g4 + g5
#
#   cee_2 <- (sum(JustDesertsDataset_copy$y) - sum_g) /
#     nrow(JustDesertsDataset_copy)
#
#   # calculate fair incomes
#   y1 <- g1 + cee_2
#   y2 <- g2 + cee_2
#   y3 <- g3 + cee_2
#   y4 <- g4 + cee_2
#   y5 <- g5 + cee_2
#
#   JustDesertsDataset_copy$z <- c(y1, y2, y3, y4, y5)
#
#   testthat::expect_equal(ee_2, JustDesertsDataset_copy$z)
# })



test_that("Conditional Egalitarian Fairness works", {
  y = "y"
  R = c("r1", "r2")
  NR = c("nr1", "nr2")

  cef <- condEgalFairness(data = JustDesertsDataset[1:5, ],
                       y = "y",
                       R = c("r1", "r2"),
                       NR = c("nr1", "nr2"))

  # assign everyone observation 1's responsibility vector
  JustDesertsDataset_copy <- JustDesertsDataset[1:5, ]
  JustDesertsDataset_copy$r1 <- mean(JustDesertsDataset$r1)
  JustDesertsDataset_copy$r2 <- mean(JustDesertsDataset$r2)

  # model expected incomes
  model <- stats::lm(stats::as.formula(paste0(y, " ~ ",
                                              paste(NR,
                                                    R,
                                                    sep = "+",
                                                    collapse =  "+"))),
                     # weights = weights,
                     data = JustDesertsDataset[1:5, ])
  summary(model)
  JustDesertsDataset_copy$z.pre <- predict(model,
                                           newdata = JustDesertsDataset_copy)
  JustDesertsDataset_copy$z.diff <- JustDesertsDataset_copy$y - JustDesertsDataset_copy$z.pre

  sum_y <- sum(JustDesertsDataset_copy$y)
  sum_z.diff <- sum(JustDesertsDataset_copy$z.diff)
  ccef <- (sum_y - sum_z.diff) / nrow(JustDesertsDataset_copy)
  JustDesertsDataset_copy$z <- JustDesertsDataset_copy$z.diff + ccef

  testthat::expect_equal(cef, JustDesertsDataset_copy$z)
})



test_that("Luck-Egalitarian Desert works", {
  y = "y"
  R = c("r1", "r2")
  NR = c("nr1", "nr2")

  led <- luckEgalDesert(data = JustDesertsDataset[1:5, ],
                       y = "y",
                       R = c("r1", "r2"),
                       NR = c("nr1", "nr2"))

  # assign everyone observation 1's responsibility vector
  JustDesertsDataset_copy <- JustDesertsDataset[1:5, ]
  JustDesertsDataset_copy$r1 <- JustDesertsDataset$r1[1]
  JustDesertsDataset_copy$r2 <- JustDesertsDataset$r2[1]

  # model expected incomes
  model <- stats::lm(stats::as.formula(paste0(y, " ~ ",
                                              paste(NR,
                                                    # R,
                                                    sep = "+",
                                                    collapse =  "+"))),
                     # weights = weights,
                     data = JustDesertsDataset[1:5, ])
  summary(model)
  JustDesertsDataset_copy$z.pre <- predict(model,
                                           newdata = JustDesertsDataset_copy)
  JustDesertsDataset_copy$z.diff <- JustDesertsDataset_copy$y - JustDesertsDataset_copy$z.pre
  ccle <- sum(JustDesertsDataset_copy$z.pre) / nrow(JustDesertsDataset_copy)
  JustDesertsDataset_copy$z <- ccle + JustDesertsDataset_copy$z.diff

  testthat::expect_equal(led, JustDesertsDataset_copy$z)
})
